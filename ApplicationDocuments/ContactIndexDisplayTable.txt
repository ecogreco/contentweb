<table id="your_contacts" class="table">
    <thead>
        <tr>
            <th>@Html.DisplayNameFor(model => model.FirstName)</th>
            <th>@Html.DisplayNameFor(model => model.LastName)</th>
            <th>@Html.DisplayNameFor(model => model.Email)</th>
            <th>@Html.DisplayNameFor(model => model.PhonePrimary)</th>
            <th>@Html.DisplayNameFor(model => model.PhoneSecondary)</th>
            <th>@Html.DisplayNameFor(model => model.Birthday)</th>
            <th>@Html.DisplayNameFor(model => model.StreetAddress1)</th>
            <th>@Html.DisplayNameFor(model => model.StreetAddress2)</th>
            <th>@Html.DisplayNameFor(model => model.City)</th>
            <th>@Html.DisplayNameFor(model => model.State)</th>
            <th>@Html.DisplayNameFor(model => model.Zip)</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach (var item in Model)
        {
            <tr>
                <td>@Html.DisplayFor(modelItem => item.FirstName)</td>
                <td>@Html.DisplayFor(modelItem => item.LastName)</td>
                <td>@Html.DisplayFor(modelItem => item.Email)</td>
                <td>@Html.DisplayFor(modelItem => item.PhonePrimary)</td>
                <td>@Html.DisplayFor(modelItem => item.PhoneSecondary)</td>
                <td>@Html.DisplayFor(modelItem => item.Birthday)</td>
                <td>@Html.DisplayFor(modelItem => item.StreetAddress1)</td>
                <td>@Html.DisplayFor(modelItem => item.StreetAddress2)</td>
                <td>@Html.DisplayFor(modelItem => item.City)</td>
                <td>@Html.DisplayFor(modelItem => item.State)</td>
                <td>@Html.DisplayFor(modelItem => item.Zip)</td>
                <td>
                    @Html.ActionLink("Edit", "Edit", new { id = item.Id }) |
                    @Html.ActionLink("Details", "Details", new { id = item.Id }) |
                    @Html.ActionLink("Delete", "Delete", new { id = item.Id })
                </td>
                <td>@Html.HiddenFor(modelItem => item.UserId)</td>
            </tr>
        }
    </tbody>
</table>

