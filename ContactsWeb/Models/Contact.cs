﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ContactsWeb.Models
{
    public class Contact
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name ="Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Email Address")]
        public string Email { get; set; }
        [Display(Name ="Primary Phone Number")]
        public string PhonePrimary { get; set; }
        [Display(Name ="Secondary Phone Number")]
        public string PhoneSecondary { get; set; }
        public DateTime Birthday { get; set; }
        [Display(Name ="Street Address")]
        public string StreetAddress1 { get; set; }
        [Display(Name ="Street Address line 2")]
        public string StreetAddress2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
 
    }
}       